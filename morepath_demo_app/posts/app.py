import morepath
from deform import Form

import more.chameleon
from more.chameleon import ChameleonApp

from sqlalchemy.ext.declarative import declarative_base

from colanderalchemy import SQLAlchemySchemaNode
from sqlalchemy import Column, Integer, Text


def config(config):
    config.scan(more.chameleon, ignore=['.tests'])


session = None
Base = declarative_base()


class Post(Base):
    __tablename__ = 'post'

    id = Column(Integer, primary_key=True)
    title = Column(Text)
    content = Column(Text)


post_schema = SQLAlchemySchemaNode(Post)


class App(ChameleonApp):
    def __init__(self, sqlalchemy_session):
        global session
        session = sqlalchemy_session


@App.setting_section(section='chameleon')
def get_setting_section():
    return {'debug': True}


@App.template_directory()
def get_template_directory():
    return '.'


@App.path('')
class Posts(object):
    pass


@App.html(model=Posts, name='add', template="add_post.pt", request_method="GET")
def add_post_form(self, request):
    form = Form(post_schema, buttons=('submit', ))
    return {'form': form}


@App.html(model=Posts, name='add', template="add_post.pt", request_method="POST")
def add_post(self, request):
    fieldnames = [field.name for field in post_schema.children]
    data = [(k, v) for k, v in request.POST.items() if k in fieldnames]
    post = Post(**dict(data))
    session.add(post)
    return morepath.redirect(request.link(Posts()))


@App.html(model=Post, name='edit', request_method="GET")
def edit_post_form(self, request):
    fieldnames = [field.name for field in post_schema.children]
    appstruct = dict([(k, getattr(self, k, None)) for k in fieldnames])

    form = Form(post_schema, buttons=('submit', ))
    return form.render(appstruct)


@App.html(model=Post, name='edit', request_method="POST")
def edit_post(self, request):
    fieldnames = [field.name for field in post_schema.children]
    for k, v in request.POST.items():
        if k not in fieldnames:
            continue
        setattr(self, k, v)

    session.add(self)
    return morepath.redirect(request.link(Posts()))


@App.html(model=Posts, template='posts.pt')
def posts_listing(self, request):
    query = session.query(Post)
    return {'objects': query.all(), 'request': request, 'context': self}


@App.path(path='post/{id}', model=Post)
def get_post(id=0):
    query = session.query(Post).filter(Post.id == id)
    return query.first()


@App.html(model=Post, template='post.pt')
def post_default_view(self, request):
    return {'context': self, 'request': request, }
