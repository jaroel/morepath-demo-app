import morepath

import more.transaction
from more.transaction import TransactionApp

import sqlalchemy
from sqlalchemy.orm import scoped_session, sessionmaker
from zope.sqlalchemy import register

from .posts.app import App as PostsApp
from .posts.app import Base, config as config_posts_app

from .zodb.app import App as ZodbApp
from .zodb.app import config as config_zodb_app

session = None


class App(TransactionApp):
    pass


@App.path('')
class Root(object):
    pass


@App.html(model=Root)
def default_view(self, request):
    posts_app_link = request.link(PostsApp(None))
    zodb_app_link = request.link(ZodbApp())
    return """
<a href="{posts_app_link}">Posts</a><br />
<a href="{zodb_app_link}">ZODB</a><br />
    """.format(**locals())


@App.mount(path='zodb', app=ZodbApp)
def mount_zodb():
    return ZodbApp()


@App.mount(path='posts', app=PostsApp)
def mount_posts():
    return PostsApp(sqlalchemy_session=session)


def main():
    global session
    session = scoped_session(sessionmaker())
    register(session)

    engine = sqlalchemy.create_engine('sqlite:///morepath_sqlalchemy.db')
    session.configure(bind=engine)

    Base.metadata.create_all(engine)
    Base.metadata.bind = engine

    config = morepath.setup()
    config.scan()
    config_posts_app(config)
    config_zodb_app(config)
    config.scan(more.transaction, ignore=['.tests'])
    config.commit()

    morepath.run(App())


if __name__ == '__main__':
    main()
