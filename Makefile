help:
	@echo Available options: it_work


# Commands

it_work: bin/morepath_demo_app
	bin/morepath_demo_app


# Install depencies

bin/morepath_demo_app: bin/buildout
	bin/buildout -v

bin/buildout: bin/pip
	bin/pip install zc.buildout

bin/pip:
	rm bin/python|true
	virtualenv -p `which python2.7` --clear .
	bin/pip install -u setuptools
	bin/pip install -u pip
