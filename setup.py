from setuptools import setup


setup(
    name='morepath_demo_app',
    version='0.1',
    description='The worlds worst demo app ever.',
    url='https://bitbucket.org/jaroel/morepath-demo-app',
    author='Roel Bruggink',
    author_email='roel@jaroel.nl',
    license='MIT',
    packages=['morepath_demo_app'],
)
